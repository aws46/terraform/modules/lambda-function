variable "pipeline_environment" {
  type = string
}
variable "prefix" {
  type    = string
  default = ""
}

variable "aws_region" {
  default = "eu-west-1"
}

variable "environment_variables" {
  type        = map(string)
  default     = {}
}

variable "tags" {
  type = map(string)
}

#Lambda variables
variable "timeout" {
  default = 30
}
variable "memory_size" {
  default = 128
}

#function name and paths
variable "lambda_function_name" {
  type    = string
  default = null
}

variable "lambda_handler_file_path" {
  type    = string
  default = null
}

variable "lambda_handler_zip_file_destination" {
  type = string
}

variable "lambda_handler_zip_file_name" {
  type = string
}

variable "handler" {
  type    = string
  default = "main"
}

variable "runtime" {
  type    = string
  default = "go1.x"
}

#vpc properties
variable "vpc_subnet_ids" {
  type    = list(string)
  default = null
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "security groups of vpc to use. Max #of them can be 5 due to the "
  default     = null
}

#PERMISSIONS
variable "iam_boundary" {
  type    = string
  default = null
}

variable "statements" {
  type = list(object(
    {
      sid       = string
      effect    = string
      resources = list(string)
      action    = list(string)
    }
  ))

  default = null
}

variable "policy_path" {
  type    = string
  default = null
}

#LOG GROUP VARIABLES
variable "log_retention_period" {
  default = 14
}

#BOOLEAN
variable "is_zip_created_by_module" {
  type    = bool
  default = true
}

locals {
  service_names = {
    role      = "role"
    policy    = "plcy"
    from_file = "file"
  }

  lambda_function_name = var.lambda_function_name == null ? join("-", [var.prefix, var.pipeline_environment]) : var.lambda_function_name
  //environment_map      = var.environment_variables[*]

  connect_to_a_vpc = var.vpc_subnet_ids != null && var.vpc_security_group_ids != null ? true : false
}