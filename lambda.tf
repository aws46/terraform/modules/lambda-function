
resource "aws_lambda_function" "lambda_function" {

  depends_on = [null_resource.create_zip, aws_cloudwatch_log_group.lambda_logs]

  function_name = local.lambda_function_name
  handler       = var.handler
  role          = aws_iam_role.lambda_execution_role.arn
  runtime       = var.runtime

  filename         = "${var.lambda_handler_zip_file_destination}/${var.lambda_handler_zip_file_name}"
  source_code_hash = var.is_zip_created_by_module ? data.archive_file.lambda_zip[0].output_base64sha256 : filebase64sha256("${var.lambda_handler_zip_file_destination}/${var.lambda_handler_zip_file_name}")


  timeout     = var.timeout
  memory_size = var.memory_size

/*  dynamic "environment" {
    for_each = local.environment_map
    content {
      variables = var.environment_variables
    }
  }*/

  dynamic "environment" {
    for_each = length(keys(var.environment_variables)) == 0 ? [] : [true]
    content {
      variables = var.environment_variables
    }
  }

  dynamic "vpc_config" {
    for_each = var.vpc_subnet_ids != null && var.vpc_security_group_ids != null ? [true] : []
    content {
      security_group_ids = var.vpc_security_group_ids
      subnet_ids         = var.vpc_subnet_ids
    }
  }

  tags = var.tags
}


data "archive_file" "lambda_zip" {
  count = var.is_zip_created_by_module ? 1 : 0

  depends_on = [null_resource.build_lambda]

  type        = "zip"
  source_file = "${var.lambda_handler_zip_file_destination}/main"
  output_path = "${var.lambda_handler_zip_file_destination}/${var.lambda_handler_zip_file_name}"
}

resource "null_resource" "build_lambda" {
  count = var.is_zip_created_by_module ? 1 : 0

  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command     = "${path.module}/scripts/build_lambda.sh ${var.lambda_handler_zip_file_destination} ${var.lambda_handler_file_path}"
    interpreter = ["bash", "-c"]
  }
}


resource "null_resource" "create_zip" {
  count = var.is_zip_created_by_module ? 1 : 0

  depends_on = [data.archive_file.lambda_zip, null_resource.build_lambda]

  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "%GOPATH%\\bin\\build-lambda-zip.exe -o ${var.lambda_handler_zip_file_destination}/${var.lambda_handler_zip_file_name} ${var.lambda_handler_zip_file_destination}/main"
  }
}


resource "aws_cloudwatch_log_group" "lambda_logs" {
  depends_on = [aws_iam_role_policy_attachment.cloudwatch_policy_attachment]

  name              = "/aws/lambda/${local.lambda_function_name}"
  retention_in_days = var.log_retention_period

  tags = var.tags
}
