output "lambda_function_name" {
  value = aws_lambda_function.lambda_function.function_name
}

output "lambda_function_arn" {
  value = aws_lambda_function.lambda_function.arn
}

output "lambda_function_id" {
  value = aws_lambda_function.lambda_function.id
}

output "lambda_function_invoke_arn" {
  value = aws_lambda_function.lambda_function.invoke_arn
}

output "lambda_function_vpc_config" {
  value = aws_lambda_function.lambda_function.vpc_config
}

output "user_defined_lambda_policy_name" {
  value = var.policy_path == null ? join("", aws_iam_policy.lambda_function_role_policy.*.name) : join("", aws_iam_policy.lambda_policy_from_file.*.name)
}

output "lambda_policy_document_json" {
  value = var.policy_path == null ? join("", data.aws_iam_policy_document.policy_document.*.json) : null
}

output "lambda_cloud_watch_log_group_name" {
  value = aws_cloudwatch_log_group.lambda_logs.name
}

