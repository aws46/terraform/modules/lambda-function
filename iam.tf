
resource "aws_iam_role" "lambda_execution_role" {
   name   = "${local.lambda_function_name}-${local.service_names.role}"
  assume_role_policy   = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  permissions_boundary = var.iam_boundary
  tags                 = var.tags

  force_detach_policies = true
}

#ATTACH POLICIES INJECTED BY THE USER
resource "aws_iam_role_policy_attachment" "role_policy_attachment" {
  count = var.statements == null ? 0 : 1

  policy_arn = join("", aws_iam_policy.lambda_function_role_policy.*.arn)
  role       = aws_iam_role.lambda_execution_role.name
}

resource "aws_iam_role_policy_attachment" "role_policy_from_file" {
  count = var.policy_path == null ? 0 : 1

  policy_arn = join("", aws_iam_policy.lambda_policy_from_file.*.arn)
  role = aws_iam_role.lambda_execution_role.name
}

#could be used when policy is given as json file
resource "aws_iam_policy" "lambda_policy_from_file" {
  count  = var.policy_path == null ? 0 : 1

  name   = "${local.lambda_function_name}-userDefinedPolicyFromFile"
  policy = file(var.policy_path)
}

resource "aws_iam_policy" "lambda_function_role_policy" {
  count = var.statements == null ? 0 : 1

  name   = "${local.lambda_function_name}-userDefinedPolicy"

  policy = data.aws_iam_policy_document.policy_document[0].json
}

data "aws_iam_policy_document" "policy_document" {
  count = var.statements == null ? 0 : 1

  dynamic "statement" {

    for_each = var.statements
    content {
      sid       = statement.value["sid"]
      effect    = statement.value["effect"]
      resources = statement.value["resources"]
      actions   = statement.value["action"]
    }
  }
}

#ATTACH CLOUDWATCH PERMISSIONS
resource "aws_iam_role_policy_attachment" "cloudwatch_policy_attachment" {

  policy_arn = aws_iam_policy.cloud_watch_iam_policy.arn
  role       = aws_iam_role.lambda_execution_role.name
}

resource "aws_iam_policy" "cloud_watch_iam_policy" {
  name   = "${local.lambda_function_name}-cloudwatch-policy"
  policy = data.aws_iam_policy_document.cloud_watch_log_policy_document.json
}

data "aws_iam_policy_document" "cloud_watch_log_policy_document" {
  statement {
    sid       = "${var.prefix}AllowLogging"
    effect    = "Allow"
    resources = ["arn:aws:logs:${var.aws_region}:*:*"]
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
  }
}

#ATTACH VPC CONNECTION RELATED PERMISSIONS
#https://docs.aws.amazon.com/lambda/latest/dg/configuration-vpc.html#vpc-permissions
data "aws_iam_policy_document" "vpc_connection_policy_document_with_subnet_condition" {
  count = local.connect_to_a_vpc ? 1 : 0

  statement {
    sid = "${var.prefix}AccessToSpecificSubnets"
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface"
    ]
    effect    = "Allow"
    resources = ["*"]
    condition {
      test     = "ForAllValues:StringEquals"
      values   = var.vpc_subnet_ids
      variable = "lambda:SubnetIds"
    }
  }
}

resource "aws_iam_policy" "vpc_connection_policy_with_subnet_criteria" {
  count = local.connect_to_a_vpc ? 1 : 0

  name   = "${local.lambda_function_name}-vpc-connection-policy"
  policy = join("", data.aws_iam_policy_document.vpc_connection_policy_document_with_subnet_condition.*.json)

}

resource "aws_iam_role_policy_attachment" "vpc_connection_policy_attachment" {
  count = local.connect_to_a_vpc ? 1 : 0

  policy_arn = join("", aws_iam_policy.vpc_connection_policy_with_subnet_criteria.*.arn)
  role       = aws_iam_role.lambda_execution_role.name
}


/*
 #Using a Service policy instead of writing yourself
  data "aws_iam_policy" "vpc_AWSLambdaENIManagementAccess" {
  count = local.connect_to_a_vpc ? 1 : 0

  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaENIManagementAccess_attach" {
  count = local.connect_to_a_vpc ? 1 : 0

  //policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
  role = aws_iam_role.lambda_execution_role.name
}
*/

