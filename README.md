* This module creates the zip file of the lambda function itself as a default behavior.
  When you would like to inject an already available zip file, then you should disable this by setting the 
  variable `is_zip_created_by_module` to false.

* `aws_iam_policy_document` is created based on the variable `statements`. 

## TODO:
- [X]  it should be possible to give the policy document as json. 
- [ ] `aws_lambda_permission` should be implemented
- [ ] creation of the rule `aws_iam_role` should be optional. If `aws_iam_role` is not created by the module, 
name of the execution role to be used must be injected into the module.

https://github.com/terraform-aws-modules/terraform-aws-lambda/blob/master/main.tf
https://lumigo.io/aws-lambda-deployment/aws-lambda-terraform/

# AWS Documentation to read
* [Lambda execution roles permissions](https://docs.aws.amazon.com/lambda/latest/dg/configuration-vpc.html#vpc-permissions)
* [Describe security group command and filters to apply for filtering aws_security_groups](https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-security-groups.html)