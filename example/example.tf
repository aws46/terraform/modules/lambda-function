provider "aws" {
  region = var.aws_region
}

module "lambda_function" {
 // source = "../../../modules/lambda-function"
  source = "./"

  lambda_handler_file_path = "../main/main.go"
  //lambda_handler_zip_file_path = "./bin/main.zip"
  lambda_handler_zip_file_destination = "./bin"
  lambda_handler_zip_file_name        = "main.zip"
  is_zip_created_by_module            = true

  tags                 = { NAME = "doing my best" }
  pipeline_environment = "dev"
  prefix               = var.prefix

  statements = [
    {
      sid       = "${var.prefix}AllowSQSPermissions"
      effect    = "Allow"
      resources = ["*"]
      action = [
        "sqs:ChangeMessageVisibility",
        "sqs:DeleteMessage",
        "sqs:GetQueueAttributes",
        "sqs:ReceiveMessage",
      ]
    },
    {
      sid       = "${var.prefix}AllowInvokingLambdas"
      effect    = "Allow"
      resources = ["arn:aws:lambda:${var.aws_region}:*:function:*"]
      action = [
        "lambda:InvokeFunction"
      ]
    }
  ]



}