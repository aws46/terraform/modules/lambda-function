#!/bin/bash

#Creates the zip file for lambda function and puts it under the bin folder

ECHO "creating ${1} if not exists"

mkdir -p "${1}"

ECHO "building"

#mklambda
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o "${1}/main" "$2"

#ECHO "creating zip"
# zip -j main.zip main
#build-lambda-zip.exe -o "${1}/${2}" "${1}/main"

#mv -v main.zip "$destination"

printf "completion time: %s\n" "$(date)"

exit 0
